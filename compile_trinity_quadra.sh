#!/bin/bash

# make -j new-ds-reclaim-ptm-other-correia-trinity-and-quadra partial_algorithm_names="progressTest abtree ziptree hashmap RedBlack_ ext_bst_ BTree_"
make -j new-ds-reclaim-ptm-other-correia-trinity-and-quadra partial_algorithm_names="abtree abtree_ hashmap"
# make -j new-ds-reclaim-ptm-other-correia-trinity-and-quadra partial_algorithm_names=""
