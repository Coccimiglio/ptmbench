FLAGS =
LDFLAGS =
GPP = g++-10
# GPP = g++-9
GPP9 = g++-9

PHYTM_FLAGS =

### if libpapi is installed but you do not want to use it, invoke make with extra argument "has_libpapi=0"
# has_libpapi=$(shell setbench/microbench/_check_lib.sh papi)
has_libpapi=0
ifneq ($(has_libpapi), 0)
	FLAGS += -DUSE_PAPI
	LDFLAGS += -lpapi
endif

### if libnuma is installed but you do not want to use it, invoke make with extra argument "has_libnuma=0"
has_libnuma=$(shell setbench/microbench/_check_lib.sh numa)
ifneq ($(has_libnuma), 0)
	FLAGS += -DUSE_LIBNUMA
	LDFLAGS += -lnuma
endif

use_asan=0
ifneq ($(use_asan), 0)
	LDFLAGS += -fsanitize=address -static-libasan
endif

use_asserts=0
ifeq ($(use_asserts), 0)
	FLAGS += -DNDEBUG
endif

use_fopenmp=0
ifeq ($(use_fopenmp), 1)
	FLAGS += -fopenmp
endif

use_timelines=0
ifeq ($(use_timelines), 1)
	FLAGS += -DMEASURE_TIMELINE_STATS
endif

hang_for_debug=0
ifeq ($(hang_for_debug), 1)
	FLAGS += -DHANG_ON_TIME_UP_FAILURE
endif

# has_htm=0
has_htm=$(shell ./checkRTM.sh)
$(info $has_htm)
ifneq ($(has_htm), 1)
	FLAGS += -DDISABLE_HTM
endif

# hash_pmem=0
hash_pmem=$(shell ./checkNVM.sh)
ifneq ($(hash_pmem), 1)
	FLAGS += -DDISABLE_PMEM
endif

use_max_abort_handler=0
ifeq ($(use_max_abort_handler), 1)
	PHYTM_FLAGS += -DUSE_MAX_ABORT_HANDLER
endif

disable_phytm_backoff=1
ifeq ($(disable_phytm_backoff), 1)
	FLAGS += -DDISABLE_PHYTM_BACKOFF
	FLAGS += -DMAX_RETRIES=100000000
else
	# FLAGS += -DMAX_RETRIES=10000
endif

no_zeroing_for_phytm=1
ifeq ($(no_zeroing_for_phytm), 1)
	FLAGS += -DDISABLE_ZEROING_MEMORY
endif



no_optimize=0
ifeq ($(no_optimize), 1)	
	FLAGS += -O0
	FLAGS += -fno-inline-functions -fno-inline-functions-called-once -fno-optimize-sibling-calls
	FLAGS += -fno-default-inline -fno-inline
	FLAGS += -fno-omit-frame-pointer
else
	# FLAGS += -O3
	FLAGS += -O2
	# FLAGS += -O1
endif
FLAGS += -mcx16 #needed for DWCAS (cmpxchg16b)

FLAGS += -DMAX_THREADS_POW2=128
FLAGS += -DCPU_FREQ_GHZ=2.1 #$(shell ./experiments/get_cpu_ghz.sh)
FLAGS += -DMEMORY_STATS=if\(0\) -DMEMORY_STATS2=if\(0\)

CFLAGS = -std=c++17 -g


FLAGS += -DNO_CLEANUP_AFTER_WORKLOAD ### avoid executing data structure destructors, to save teardown time at the end of each trial (useful with massive trees)
# FLAGS += -DRAPID_RECLAMATION
FLAGS += -DRECORD_ABORTS
FLAGS += -DGSTATS_MAX_THREAD_BUF_SIZE=524288
FLAGS += -DDEBRA_ORIGINAL_FREE
FLAGS += $(xargs)

LDFLAGS += -Lsetbench/lib
LDFLAGS += -I. -Isetbench/microbench `find setbench/common -type d | sed s/^/-I/`
LDFLAGS += -lpthread
LDFLAGS += -latomic
LDFLAGS += -ldl
LDFLAGS += -mrtm

.PHONY: all
all:

bin_dir=bin
dir_guard:
	@mkdir -p $(bin_dir)

clean:
	rm $(bin_dir)/*.out

################################################################################
#### DECIDING WHICH COMBINATIONS OF: {DATA STRUCTURE, RECLAIMER, TM} TO USE
################################################################################

$(info ------------------------------------)

# RECLAIMERS=debra none
RECLAIMERS=none

partial_algorithm_names=
ifeq ($(partial_algorithm_names),)
    SETBENCH_DATA_STRUCTURES=$(patsubst setbench/ds/%/adapter.h,%,$(wildcard setbench/ds/*/adapter.h))
else
	SETBENCH_DATA_STRUCTURES=$(foreach partial_name,$(partial_algorithm_names), $(patsubst setbench/ds/%/adapter.h,%,$(wildcard setbench/ds/$(partial_name)*/adapter.h)))
endif

define create-target-ds-reclaim =
$(1).$(2): dir_guard
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2) -Isetbench/ds/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) $(CFLAGS) $(LDFLAGS)
ds-reclaim: $(1).$(2)
endef
$(foreach ds,$(SETBENCH_DATA_STRUCTURES), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(eval $(call create-target-ds-reclaim,$(ds),$(reclaim))) \
	) \
)

PTM_ALGS=
ifeq ($(PTM_ALGS),)
	PTM_ALGS=$(patsubst ptmlib/%/stm.h,%,$(wildcard ptmlib/*/stm.h))
endif

partial_algorithm_names=
ifeq ($(partial_algorithm_names),)
    NEW_PTM_DATA_STRUCTURES=$(patsubst ds_ptm/%/adapter.h,%,$(wildcard ds_ptm/*/adapter.h))
else
	NEW_PTM_DATA_STRUCTURES=$(foreach partial_name,$(partial_algorithm_names), $(patsubst ds_ptm/%/adapter.h,%,$(wildcard ds_ptm/$(partial_name)*/adapter.h)))
endif
define create-target-new-ds-reclaim-ptm =
_$(1).$(2).$(3): dir_guard
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).$(3) -DUSE_GSTATS_USER_HANDLER_H_FILE -Iptmlib -Ids_ptm/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) -D$(3) $(FLAGS) $(CFLAGS) $(LDFLAGS)
new-ds-reclaim-ptm: _$(1).$(2).$(3)
endef
$(foreach ds,$(NEW_PTM_DATA_STRUCTURES), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(foreach ptm,$(PTM_ALGS), \
			$(eval $(call create-target-new-ds-reclaim-ptm,$(ds),$(reclaim),$(ptm))) \
		) \
	) \
)
all: new-ds-reclaim-ptm






PHYTM_ALGS=
ifeq ($(PHYTM_ALGS),)
	PHYTM_ALGS=$(patsubst phytmlib/%/stm.h,%,$(wildcard phytmlib/*/stm.h))
endif
partial_algorithm_names=
ifeq ($(partial_algorithm_names),)
    NEW_PHYTM_DATA_STRUCTURES=$(patsubst ds_phytm/%/adapter.h,%,$(wildcard ds_phytm/*/adapter.h))
else
	NEW_PHYTM_DATA_STRUCTURES=$(foreach partial_name,$(partial_algorithm_names), $(patsubst ds_phytm/%/adapter.h,%,$(wildcard ds_phytm/$(partial_name)*/adapter.h)))
endif
define create-target-new-ds-reclaim-phytm =
_$(1).$(2).$(3): dir_guard
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).$(3) -DUSE_GSTATS_USER_HANDLER_H_FILE -DTM_DS -Iphytmlib -Ids_phytm/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) -D$(3) $(FLAGS) $(PHYTM_FLAGS) $(CFLAGS) $(LDFLAGS)
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).$(3).free_disabled -DDISABLE_TM_ALLOC_FREE -DTM_DS -DUSE_GSTATS_USER_HANDLER_H_FILE -Iphytmlib -Ids_phytm/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) -D$(3) $(FLAGS) $(PHYTM_FLAGS) $(CFLAGS) $(LDFLAGS)
new-ds-reclaim-phytm: _$(1).$(2).$(3)
endef
$(foreach ds,$(NEW_PHYTM_DATA_STRUCTURES), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(foreach phytm,$(PHYTM_ALGS), \
			$(eval $(call create-target-new-ds-reclaim-phytm,$(ds),$(reclaim),$(phytm))) \
		) \
	) \
)
all: new-ds-reclaim-phytm






SPHT_GCC = gcc
SPHT_CFLAGS = -std=c++11 -Wall -fpermissive -g -w 
SPHT_DIR = spht
SPHT_NVHTM_SRC_DIR = $(SPHT_DIR)/nvhtm/src
# SPHT_LIBS = 
SPHT_LIBS = -lhtm_sgl
# SPHT_LIBS += -ltcmalloc 
SPHT_LIBS += -lrt -mrtm -lm 
SPHT_DEFS += -DNPROFILE 
SPHT_DEFS += -DSPHT -DSPHT_MEASUREMENT_INCLUDE_REPLAY -DTM_DS
SPHT_INCLUDES = -I ds_spht/$(1) -I $(SPHT_DIR) -I $(SPHT_DIR)/nvhtm/include -I $(SPHT_DIR)/deps/htm_alg/include -I $(SPHT_DIR)/deps/arch_dep/include -L $(SPHT_DIR)/deps/htm_alg -L $(SPHT_DIR)/nvhtm 
ifeq ($(no_optimize), 1)	
	SPHT_CFLAGS += -O0
	SPHT_CFLAGS += -fno-inline-functions -fno-inline-functions-called-once -fno-optimize-sibling-calls
	SPHT_CFLAGS += -fno-default-inline -fno-inline
	SPHT_CFLAGS += -fno-omit-frame-pointer
else
	# SPHT_CFLAGS += -O3
	SPHT_CFLAGS += -O2
	# SPHT_CFLAGS += -O1
endif

partial_algorithm_names=
ifeq ($(partial_algorithm_names),)
    SPHT_DS=$(patsubst ds_spht/%/adapter.h,%,$(wildcard ds_spht/*/adapter.h))
else
	SPHT_DS=$(foreach partial_name,$(partial_algorithm_names), $(patsubst ds_spht/%/adapter.h,%,$(wildcard ds_spht/$(partial_name)*/adapter.h)))
endif

define create-target-new-ds-reclaim-spht =
_$(1).$(2).spht: dir_guard		
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).spht -lnvhtm $(SPHT_DEFS) $(SPHT_INCLUDES) -DUSE_GSTATS_USER_HANDLER_H_FILE -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) $(PHYTM_FLAGS) $(CFLAGS) $(LDFLAGS) $(SPHT_LIBS)
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).spht_noopf -lnvhtm_noopf $(SPHT_DEFS) $(SPHT_INCLUDES) -DUSE_GSTATS_USER_HANDLER_H_FILE -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) $(PHYTM_FLAGS) $(CFLAGS) $(LDFLAGS) $(SPHT_LIBS)
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).spht_no_pmem -lnvhtm_no_pmem $(SPHT_DEFS) $(SPHT_INCLUDES) -DUSE_GSTATS_USER_HANDLER_H_FILE -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) $(PHYTM_FLAGS) $(CFLAGS) $(LDFLAGS) $(SPHT_LIBS)
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).spht_no_phtm -lnvhtm_no_phtm $(SPHT_DEFS) $(SPHT_INCLUDES) -DUSE_GSTATS_USER_HANDLER_H_FILE -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) $(PHYTM_FLAGS) $(CFLAGS) $(LDFLAGS) $(SPHT_LIBS)
new-ds-reclaim-spht: _$(1).$(2).spht
endef
$(foreach ds,$(SPHT_DS), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(eval $(call create-target-new-ds-reclaim-spht,$(ds),$(reclaim),spht)) \
	) \
)
all: new-ds-reclaim-spht






partial_algorithm_names=
ifeq ($(partial_algorithm_names),)
    PTM_OTHER_CORREIA_DATA_STRUCTURES=$(patsubst ds_ptm_correia_redoopt/%/adapter.h,%,$(wildcard ds_ptm_correia_redoopt/*/adapter.h))
else
	PTM_OTHER_CORREIA_DATA_STRUCTURES=$(foreach partial_name,$(partial_algorithm_names), $(patsubst ds_ptm_correia_redoopt/%/adapter.h,%,$(wildcard ds_ptm_correia_redoopt/$(partial_name)*/adapter.h)))
endif
define create-target-new-ds-reclaim-ptm-other-correia-redoopt =
_$(1).$(2).redoopt: dir_guard
	$(GPP) otherPtms/correia-cx-and-redo/common/ThreadRegistry.cpp otherPtms/correia-cx-and-redo/redoopt/RedoOpt.cpp setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).redoopt -DUSE_GSTATS_USER_HANDLER_H_FILE -DPWB_IS_CLFLUSHOPT -IotherPtms/correia-cx-and-redo -Ids_ptm_correia_redoopt/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) $(CFLAGS) $(LDFLAGS)	
new-ds-reclaim-ptm-other-correia-redoopt: _$(1).$(2).redoopt
endef
$(foreach ds,$(PTM_OTHER_CORREIA_DATA_STRUCTURES), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(eval $(call create-target-new-ds-reclaim-ptm-other-correia-redoopt,$(ds),$(reclaim),redoopt)) \
	) \
)
# all: new-ds-reclaim-ptm-other-correia-redoopt







partial_algorithm_names=
ifeq ($(partial_algorithm_names),)
    PTM_OTHER_CORREIA_DATA_STRUCTURES_B=$(patsubst ds_ptm_correia_trinity_and_quadra/%/adapter.h,%,$(wildcard ds_ptm_correia_trinity_and_quadra/*/adapter.h))
else
	PTM_OTHER_CORREIA_DATA_STRUCTURES_B=$(foreach partial_name,$(partial_algorithm_names), $(patsubst ds_ptm_correia_trinity_and_quadra/%/adapter.h,%,$(wildcard ds_ptm_correia_trinity_and_quadra/$(partial_name)*/adapter.h)))
endif
define create-target-new-ds-reclaim-ptm-other-correia-trinity-and-quadra =
_$(1).$(2).trinity_quadra: dir_guard	
	$(GPP9) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).trinity_quadra -DTRINITY -DUSE_GSTATS_USER_HANDLER_H_FILE -DPWB_IS_CLFLUSHOPT -IotherPtms/correia-cx-and-redo -Ids_ptm_correia_trinity_and_quadra/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) -std=c++14 -g $(LDFLAGS)
new-ds-reclaim-ptm-other-correia-trinity-and-quadra: _$(1).$(2).trinity_quadra
endef
$(foreach ds,$(PTM_OTHER_CORREIA_DATA_STRUCTURES_B), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(eval $(call create-target-new-ds-reclaim-ptm-other-correia-trinity-and-quadra,$(ds),$(reclaim),trinity_quadra)) \
	) \
)
# all: new-ds-reclaim-ptm-other-correia-trinity-and-quadra








# TM_ALGS=$(patsubst tmlib/%/stm.h,%,$(wildcard tmlib/*/stm.h))
partial_algorithm_names=
ifeq ($(partial_algorithm_names),)
    VOLATILE_TM_ALGS=$(patsubst ds_tm/%/adapter.h,%,$(wildcard ds_tm/*/adapter.h))
else
	VOLATILE_TM_ALGS=$(foreach partial_name,$(partial_algorithm_names), $(patsubst ds_tm/%/adapter.h,%,$(wildcard ds_tm/$(partial_name)*/adapter.h)))
endif
TM_ALGS=hytm3
define create-target-new-ds-reclaim-tm =
_$(1).$(2).$(3): dir_guard
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).$(3) -DUSE_GSTATS_USER_HANDLER_H_FILE -Itmlib -Ids_tm/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) -D$(3) $(FLAGS) $(CFLAGS) $(LDFLAGS)
new-ds-reclaim-tm: _$(1).$(2).$(3)
endef
$(foreach ds,$(VOLATILE_TM_ALGS), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(foreach tm,$(TM_ALGS), \
			$(eval $(call create-target-new-ds-reclaim-tm,$(ds),$(reclaim),$(tm))) \
		) \
	) \
)
# all: new-ds-reclaim-tm







NEW_DATA_STRUCTURES=$(patsubst ds/%/adapter.h,%,$(wildcard ds/*/adapter.h))
define create-target-new-ds-reclaim =
_$(1).$(2): dir_guard
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2) -Ids/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) -DUSE_GSTATS_USER_HANDLER_H_FILE $(FLAGS) $(CFLAGS) $(LDFLAGS)
new-ds-reclaim: _$(1).$(2)
endef
$(foreach ds,$(NEW_DATA_STRUCTURES), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(eval $(call create-target-new-ds-reclaim,$(ds),$(reclaim))) \
	) \
)
# all: new-ds-reclaim