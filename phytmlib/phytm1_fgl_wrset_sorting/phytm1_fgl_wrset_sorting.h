#pragma once

//#ifdef __cplusplus
//extern "C" {
//#endif

#define TM_NAME "phytm1_fgl_and_rdwrset_sorting"


#ifndef MAX_FALLBACK_RETRIES_BEFORE_SORTING
#define MAX_FALLBACK_RETRIES_BEFORE_SORTING 100
#endif

#define ENABLE_WRITE_SET_SORTING 1

#include "../phytmCommon/tm.h"

// #include "../phytmCommon/common.h"

//#ifdef __cplusplus
//}
//#endif
