#pragma once

//#ifdef __cplusplus
//extern "C" {
//#endif

#define TM_NAME "phytm1_fgl_always_sorting_strong_progressive"

#define ENABLE_WRITE_SET_SORTING 1
#define FORCE_WRITE_SET_SORTING 1
#define STRONG_PROG 1

#include "../phytmCommon/tm.h"

// #include "../phytmCommon/common.h"

//#ifdef __cplusplus
//}
//#endif
