#pragma once


// #define ABORT_ADDR_MASK 0xFFFF0000000


// enum abortType {
// 	TX_COMMIT_READ_SET_VALIDATION_FAILURE = 1,
// 	TX_COMMIT_WRTIE_SET_FAILED_LOCK_ACQ = 2,
// 	TX_LOAD_ADDR_LOCKED = 4,
// 	TX_LOAD_READ_SET_VALIDATION_FAILURE = 8
// };


#ifndef HTM_GLOBAL_LOCK_ABORT_CODE
    #define HTM_GLOBAL_LOCK_ABORT_CODE 1
#endif

#ifndef HTM_WRITE_SET_CAPACITY_ABORT_CODE
    #define HTM_WRITE_SET_CAPACITY_ABORT_CODE 2
#endif

#ifndef NON_PERSISTENT_READ_ABORT_CODE
    #define NON_PERSISTENT_READ_ABORT_CODE 3
#endif

#ifndef HTM_FAILED_ADDR_LOCK_ACQUISITION_ABORT_CODE
    #define HTM_FAILED_ADDR_LOCK_ACQUISITION_ABORT_CODE 4
#endif